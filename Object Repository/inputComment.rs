<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>inputComment</name>
   <tag></tag>
   <elementGuidId>39c5d5be-72b5-4077-b80e-f1400b702337</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#txt_comment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='txt_comment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>81d6759f-60a6-4c39-ae91-cf3afa894f23</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>113deb34-2892-4bbd-be45-307951b5e26b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txt_comment</value>
      <webElementGuid>f2138d05-db30-4fc4-b059-bf2a76a51b3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>comment</value>
      <webElementGuid>2d1932fe-d732-4ad5-9904-b8d8bf0328e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Comment</value>
      <webElementGuid>f1188fd1-94a8-40a4-91a9-74495cd76a48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>82a09bea-67f3-4c9b-9ddd-a281a2854276</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;txt_comment&quot;)</value>
      <webElementGuid>dce09986-c3d1-43f2-8b8c-c848176c6da7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='txt_comment']</value>
      <webElementGuid>b72368b9-89fa-42e7-90f7-4554fc80f13d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[5]/div/textarea</value>
      <webElementGuid>ddb54f9e-5174-4c74-88bf-bcfa5733f147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>b61651b4-991b-4d86-b4c7-337591fc6146</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@id = 'txt_comment' and @name = 'comment' and @placeholder = 'Comment']</value>
      <webElementGuid>569ea3b6-525b-4786-aa16-83bf7f87d9ec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
